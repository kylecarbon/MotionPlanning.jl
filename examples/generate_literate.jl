using Literate

OUTPUT = joinpath(@__DIR__, "generated/")

poly_opt_example = joinpath(@__DIR__, "polynomial_optimisation.jl")
Literate.markdown(poly_opt_example, OUTPUT; credit = false, documenter = true)
Literate.notebook(poly_opt_example, OUTPUT; credit = false)

path_planning_example = joinpath(@__DIR__, "path_planning.jl")
Literate.markdown(path_planning_example, OUTPUT; credit = false, documenter = true)
Literate.notebook(path_planning_example, OUTPUT; credit = false)