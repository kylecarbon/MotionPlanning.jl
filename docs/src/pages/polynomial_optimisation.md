# Polynomial Optimisation
```@index
Pages = ["polynomial_optimisation.md"]
```
## Benchmarks
### Varying number of segments
```@example 1
import MotionPlanning # hide
const mp = MotionPlanning # hide
using PlotlyJS # hide
using ORCA # hide
dim = 3
num_coefficients = 12
results = mp.polyopt_benchmark_segment(max_segments = 10, dim = dim, num_coefficients = num_coefficients)
p_time = mp.plot_benchmark(results, test_name = "segment",
                           benchmark_field = :time,
                           title = "Solve time vs. number of segments for Dimension = $dim, N = $(num_coefficients)",
                           xaxis_title = "Number of segments",
                           yaxis_title = "Median solve time [μs]")
p_memory = mp.plot_benchmark(results, test_name = "segment",
                             benchmark_field = :memory,
                             title = "Memory allocated vs. number of segments for Dimension = $dim, N = $(num_coefficients)",
                             xaxis_title = "Number of segments",
                             yaxis_title = "Median memory allocated [MiB]")
savefig(p_time, "segment-plot-time.svg"); nothing # hide
savefig(p_memory, "segment-plot-memory.svg"); nothing # hide
```
![segment-plot-time](segment-plot-time.svg)
![segment-plot-memory](segment-plot-memory.svg)

## Public API
```@docs
MotionPlanning.PolyOpt
MotionPlanning.Vertex
MotionPlanning.dimension
MotionPlanning.getN
MotionPlanning.setN!
MotionPlanning.setup
MotionPlanning.setup!
MotionPlanning.solveLinear
```