@testset "Path" begin
    @testset "Path2D Arclength" begin
        points = Vector{mp.Point2D}()
        for i = 1:10
            push!(points, mp.Point2D(i))
        end
        a = mp.Path2D(points)
        @test mp.arclength(a) ≈ 9*sqrt(2)
    end
end