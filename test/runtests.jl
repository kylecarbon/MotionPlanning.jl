# HACK: https://github.com/JuliaImages/ImageView.jl/pull/156
# this is necessary to get ImageMagick to play nicely
using ImageMagick

using Test

using MotionPlanning
const mp = MotionPlanning

import Polynomials
const po = Polynomials

using StaticArrays
const sa = StaticArrays

import Atmosphere
const am = Atmosphere

mp.greet()
@time @testset "Datatypes" begin include("datatypes_test.jl") end
@time @testset "Polynomial Optimisation" begin include("polynomial_optimisation_test.jl") end
@time @testset "Path Planning" begin include("path_planning_tests.jl") end
@time @testset "Plots" begin include("plots_test.jl") end
@time @testset "Benchmarks" begin include("benchmarks_test.jl") end
@time @testset "Examples" begin include("examples_test.jl") end
@time @testset "Dynamics" begin include("dynamics/dynamics_tests.jl") end
@time @testset "Controls" begin include("controls/controls_tests.jl") end
