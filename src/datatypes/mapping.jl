#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

abstract type AbstractEnvironment end
abstract type AbstractGridmap{DataType <: Real,CoordinateType <: AbstractFloat} <: AbstractEnvironment end

"""
    GridmapND(data::Array{DataType, Dimension}, resolution::CoordinateType, origin::PointND{Dimension, CoordinateType})

An N-dimensional gridmap, discretised such that all dimensions of a single cell have size `resolution`.
The gridmap is located at its `origin`.
"""
mutable struct GridmapND{Dimension,DataType,CoordinateType} <: AbstractGridmap{DataType,CoordinateType}
    data::Array{DataType,Dimension}
    resolution::CoordinateType
    origin::PointND{Dimension,CoordinateType}
end

"""
    Gridmap2D(data::Matrix{DataType}, resolution::CoordinateType, origin::Point2D{CoordinateType})

A 2D gridmap (see [`GridmapND`](@ref)).
"""
const Gridmap2D{DataType,CoordinateType} = GridmapND{2,DataType,CoordinateType}

Gridmap2D() = Gridmap2D{UInt8,Float64}(Array{UInt8,2}(undef, 0, 0), Float64(NaN), Point2D())

"""
    Gridmap3D(data::Matrix{DataType}, resolution::CoordinateType, origin::Point3D{CoordinateType})

A 3D gridmap (see [`GridmapND`](@ref)).
"""
const Gridmap3D{DataType,CoordinateType} = GridmapND{3,DataType,CoordinateType}

Gridmap3D() = Gridmap3D{UInt8,Float64}(Array{UInt8,3}(undef, 0, 0, 0), Float64(NaN), Point3D())

"""
    get_origin(gridmap::GridmapND)

Return a [`PointND`](@ref) that represents the map's origin.
"""
get_origin(AbstractGridmap) = error("Not defined for AbstractGridmap -- must implement for derived type.")
get_origin(gridmap::GridmapND) = gridmap.origin

"""
    get_resolution(gridmap::GridmapND)

Return the gridmap's resolution (see description of [`GridmapND`](@ref)).
"""
get_resolution(gridmap::GridmapND) = gridmap.resolution

"""
    size(gridmap::GridmapND)

Return an N-tuple of the gridmap's size.
"""
Base.size(gridmap::GridmapND) = size(gridmap.data)

"""
    getwidthcells(gridmap)

Return the width of the gridmap, i.e. the number of cells along its first dimension.
"""
getwidthcells(gridmap::GridmapND)::Int = size(gridmap.data, 1)
"""
    getheightcells(gridmap)

Return the height of the gridmap, i.e. the number of cells along its second dimension.
"""
getheightcells(gridmap::GridmapND)::Int = size(gridmap.data, 2)
"""
    getdepthcells(gridmap)

Return the depth of the gridmap, i.e. the number of cells along its third dimension.
"""
getdepthcells(gridmap::GridmapND)::Int = size(gridmap.data, 3)

"""
    getwidthmetric(gridmap::GridmapND)

Return the metric width of the gridmap.
"""
getwidthmetric(gridmap::GridmapND)::Float64 = getwidthcells(gridmap) * get_resolution(gridmap)
"""
    getheightmetric(gridmap::GridmapND)

Return the metric height of the gridmap.
"""
getheightmetric(gridmap::GridmapND)::Float64 = getheightcells(gridmap) * get_resolution(gridmap)
getdepthmeters(gridmap::GridmapND)::Float64 = getdepthcells(gridmap) * get_resolution(gridmap)

"""
"Enums" available for checking the status of the gridmap.
* `FREE`: the map is free/unoccupied
* `LETHAL`: the map is lethal/occupied
* `NO_INFORMATION`: the map is unknown
"""
module Mapping

    const FREE = 0
    const LETHAL = 255
    const NO_INFORMATION = 253

end # end mapping