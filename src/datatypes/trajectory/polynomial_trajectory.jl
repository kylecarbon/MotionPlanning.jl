#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

function polyder(poly::po.Poly, derivative_order::Integer = 1)
  if derivative_order < 0
      throw(DomainError(derivative_order, "derivative_order must be ⫺ 0."))
  end
  for i = 1:derivative_order
      poly = po.polyder(poly)
  end
  return poly
end

"""
  PolynomialTrajectory(t0, tf, t_shift, polynomials)

`[t0, tf]`: time of validity for the PolynomialTrajectory

`t_shift`: time shift for the Polynomial, i.e. the Polynomial is evaluated at `t_eval = (t_now - t_shift)`

`polynomials`: dictionary of Polynomials (a key corresponds to the dimension, e.g. the x-dimension)
"""
mutable struct PolynomialTrajectory{T <: AbstractFloat} <: AbstractTrajectory{T}
    t0::T
    tf::T
    t_shift::T
    polynomials::Dict{Int, po.Poly{T}}
    max_derivative::Int64 # maximum derivative that returns non-zero
    function PolynomialTrajectory{T}(t0::T, tf::T, t_shift::T,
                                     polynomials::Dict{Int64, po.Poly{T}}) where T
        if (tf < t0)
            error("tf < t0 -- final time cannot be less than initial time")
        end
        max_derivative = 0
        for (_, poly) ∈ polynomials
            max_derivative = max(max_derivative, length(poly))
        end
        max_derivative -= 1
        new{T}(t0, tf, t_shift, polynomials, max_derivative)
    end
end

function PolynomialTrajectory()
    poly_dict = Dict(Dimensions.X=>po.Poly(NaN), Dimensions.Y=>po.Poly(NaN), Dimensions.Z=>po.Poly(NaN))
    PolynomialTrajectory{Float64}(NaN, NaN, NaN, poly_dict)
end

function PolynomialTrajectory(end_time::Real, position::AbstractVector{T}) where {T <: AbstractFloat}
    poly_dict = Dict{Int64, po.Poly{T}}()
    for (index, entry) ∈ enumerate(position)
        poly_dict[index] = po.Poly([entry])
    end
    PolynomialTrajectory{T}(0.0, T(end_time), 0.0, poly_dict)
end

"""
  PolynomialTrajectory{T}(end_time::T, poly::po.Poly{T}) where {T <: AbstractFloat}

Convenience constructor which creates a PolynomialTrajectory, where:
1. the trajectory's start time is zero.
1. the input polynomial is automatically converted to a single-dimension trajectory.
"""
function PolynomialTrajectory(end_time::T, poly::po.Poly{T}) where {T <: AbstractFloat}
    poly_dict = Dict(Dimensions.X=>poly)
    PolynomialTrajectory{T}(0.0, T(end_time), 0.0, poly_dict)
end

"""
  PolynomialTrajectory(end_time::real, polynomials::Dict{Int64, po.Poly{T}}) where {T <: AbstractFloat}

Convenience constructor which creates a PolynomialTrajectory, with the following properties:
1. the trajectory's start time is zero.
1. the trajectory's polynomials are set to the input polynomials.
"""
function PolynomialTrajectory{T}(end_time::Real, polynomials::Dict{Int64, po.Poly{T}}) where {T <: AbstractFloat}
    PolynomialTrajectory{T}(0.0, T(end_time), 0.0, polynomials)
end
function PolynomialTrajectory(end_time::Real, polynomials::Dict{Int64, po.Poly{T}}) where {T <: AbstractFloat}
    PolynomialTrajectory{T}(0.0, T(end_time), 0.0, polynomials)
end

function PolynomialTrajectory{T}(start_time::Real, end_time::Real, poly::po.Poly) where {T <: AbstractFloat}
    poly_dict = Dict(Dimensions.X=>poly, Dimensions.Y=>poly, Dimensions.Z=>poly)
    PolynomialTrajectory{T}(T(start_time), T(end_time), T(start_time), poly_dict)
end

function PolynomialTrajectory{T}(end_time::Real, poly::po.Poly{T}) where T
    poly_dict = Dict(Dimensions.X=>poly, Dimensions.Y=>poly, Dimensions.Z=>poly)
    PolynomialTrajectory{Float64}(0.0, Float64(end_time), 0.0, convert(Dict{Int64, po.Poly{Float64}}, poly_dict))
end

function copy(traj::PolynomialTrajectory{T}) where T
    return PolynomialTrajectory{T}(traj.t0, traj.tf, traj.t_shift, traj.polynomials)
end

function Base.show(io::IO, ::MIME"text/plain", polytraj::PolynomialTrajectory)
    poly_str = ""
    for poly ∈ polytraj.polynomials
        poly_str *= "\n\t\t" * string(poly)
    end
    str = "[Polynomial Trajectory]\n" *
          "\tDimension: $(num_dimensions(polytraj))\n" *
          "\tMaximum order: $(getN(polytraj) - 1)\n" *
          "\tTime (start, stop, duration): ($(get_start_time(polytraj)), $(get_end_time(polytraj)), $(get_duration(polytraj)))\n" *
          "\tPolynomials (dimension => polynomial):"
    print(io, str, poly_str)
end

function Base.show(io::IO, polytraj::PolynomialTrajectory)
    poly_str = "\n\n[PolyTraj]\n\tTime (start, stop) = ($(get_start_time(polytraj)), $(get_end_time(polytraj)))" *
               "\n\tPolynomials (dimension => polynomial):"
    for poly ∈ polytraj.polynomials
        poly_str *= "\n\t" * string(poly)
    end
    print(io, poly_str)
end

num_dimensions(traj::PolynomialTrajectory) = length(traj.polynomials)
num_derivatives(traj::PolynomialTrajectory) = traj.max_derivative + 1
function getN(traj::PolynomialTrajectory)
    max_n = 0
    for (dimension, poly) ∈ traj.polynomials
        max_n = max(length(poly), max_n)
    end
    return max_n
end

function evaluate(traj::PolynomialTrajectory{T}, time::Real) where T
    if time < traj.t0
        time = traj.t0
    elseif time > traj.tf
        time = traj.tf
    end

    traj_dimensions = num_dimensions(traj)
    traj_derivatives = num_derivatives(traj)
    dt = time - traj.t_shift
    sm = StateMatrix(sa.MMatrix{traj_dimensions, traj_derivatives, T}(undef))
    for dim = Base.OneTo(traj_dimensions)
        poly = traj.polynomials[dim]
        for derivative = 0 : (traj_derivatives - 1)
            if derivative > 0
                poly = po.polyder(poly)
            end
            sm[dim, derivative+1] = po.polyval(poly, dt)
        end
    end
    return sm
end

function set_start_time!(traj::PolynomialTrajectory, new_time::Real)
    traj.tf += new_time - traj.t0
    traj.t0 = new_time
    traj.t_shift = new_time
    return traj
end

function slice(traj::PolynomialTrajectory{T}, t0::Real, tf::Real) where T
    if (t0 < get_start_time(traj)) || (tf > get_end_time(traj)) || (t0 > tf)
        error_msg = """Invalid bounds for slicing PolynomialTrajectory:
                    t0 = $t0, tf = $tf
                    traj.t0 = $(get_start_time(traj))
                    traj.tf = $(get_end_time(traj))
                """
        error(error_msg)
    end
    return PolynomialTrajectory{T}(T(t0), T(tf), T(traj.t_shift), traj.polynomials)
end

function derivative(traj::PolynomialTrajectory{T}, derivative_order::Int = 1) where {T <: AbstractFloat}
    if derivative_order < 0
        throw(DomainError(derivative_order, "derivative_order must be ⫺ 0 -- instead,
                                            you input derivative_order = $(derivative_order)"))
    end
    poly_deriv = Dict{Int64, po.Poly{T}}()
    for (dimension, poly) ∈ traj.polynomials
        new_poly = polyder(poly, derivative_order)
        poly_deriv[dimension] = new_poly
    end
    return PolynomialTrajectory{T}(traj.t0, traj.tf, traj.t_shift, poly_deriv)
end
