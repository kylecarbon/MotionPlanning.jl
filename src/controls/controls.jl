#=
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

"""
    AbstractRigidBodyControls

Abstract type that represents a controller for a rigid body object.
Must implement the following methods:
1. `calc_force`
1. `calc_moment`
TODO: insert ref to other functions
"""
abstract type AbstractRigidBodyController end

compute_wrench(controller::AbstractRigidBodyController) = compute_wrench(controller)
compute_force(controller::AbstractRigidBodyController) = compute_force(controller)
compute_moment(controller::AbstractRigidBodyController) = compute_moment(controller)

include("fake_controller.jl")
# include("lee_controller.jl")
