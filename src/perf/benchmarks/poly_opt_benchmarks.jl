
function simple_poly_solve(;dim = 3, N = 10, segments = 1)
    num_vertices = segments + 1
    @assert segments >= 1 "Must have at least 1 segment -- instead, received $segments."

    problem = MotionPlanning.PolyOpt(dim, N)

    start = Vertex{dim, Float64}()
    start[Derivatives.POSITION] = sa.@SVector rand(dim)
    vertices = [start]
    for i = Base.OneTo(num_vertices - 1)
        midpoint = Vertex{dim, Float64}()
        midpoint[Derivatives.POSITION] = sa.@SVector rand(dim)
        midpoint[Derivatives.VELOCITY] = sa.@SVector rand(dim)
        push!(vertices, midpoint)
    end

    times = ones(segments)
    derivative_to_optimise = min(4, Int(N / 2 - 1))
    setup!(problem, vertices, times, derivative_to_optimise)
    return solveLinear(problem)
end

function polyopt_benchmark_segment(;max_segments = 10, dim = 3, num_coefficients = 10, samples = 10000)
    suite = BenchmarkGroup()

    suite["segment"] = BenchmarkGroup(["segment"])
    for segment = Base.OneTo(max_segments)
        suite["segment"][segment] = BenchmarkTools.@benchmarkable simple_poly_solve(dim = $dim, N = $num_coefficients, segments = $segment)
    end

    return BenchmarkTools.run(suite, samples = samples)
end
