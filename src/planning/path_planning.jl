#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

"""
    get_cartesian_index(gridmap::Gridmap2D, point::Point2D)::CartesianIndex

If the point is on the map, get the cartesian index of the point on the map.
If the point is off the map, return `nothing`.
"""
function get_cartesian_index(gridmap::Gridmap2D, point::Point2D)
    if !on_map(gridmap, point)
        return nothing
    end

    dist_tol = 1e-6;
    width_m = getwidthmetric(gridmap)
    width_c = getwidthcells(gridmap)
    height_m = getheightmetric(gridmap)
    height_c = getheightcells(gridmap)
    mx = width_c
    my = height_c

    if abs(point.x - width_m) >= dist_tol
        mx = Int(floor(((point.x - gridmap.origin.x) / gridmap.resolution)) + 1)
    end
    if abs(point.y - height_m) >= dist_tol
        my = Int(floor(((point.y - gridmap.origin.y) / gridmap.resolution)) + 1)
    end

    if (mx > width_c) || (my > height_c)
        @debug "index off map: $([mx, width_c]), $([my, height_c])"
        return nothing
    end
    return CartesianIndex(mx, my)
end

"""
    get_linear_index(gridmap::Gridmap2D, point::Point2D)::LinearIndex

If the point is on the map, get the linear index of the point on the map.
If the point is off the map, return `nothing`.
"""
function get_linear_index(gridmap::Gridmap2D, point::Point2D)
    ci = get_cartesian_index(gridmap, point)
    if (ci == nothing)
        return nothing
    end

    li = LinearIndices(gridmap.data)
    return li[ci]
end

"""
    get_position(gridmap::Gridmap2D, vertex::CartesianIndex{2})::Point2D

**Precondition:** `vertex` must be on the `gridmap`.

Given a vertex, return the metric position of it on the map.
"""
function get_position(gridmap::Gridmap2D, vertex::CartesianIndex{2})::Point2D
    res = get_resolution(gridmap)
    origin = get_origin(gridmap)
    # When getting the position of a particular vertex, return the position of its center.
    # This also means when converting back from position to a CartesianIndex, due to flooring,
    # it will return the same CartesianIndex that was originally used to get the position.
    return Point2D((vertex[1] - 0.5) * res + origin.x, (vertex[2] - 0.5) * res + origin.y)
end

function has_left(gridmap::Gridmap2D, vertex::CartesianIndex{2})::Bool
    # cx, cy = Tuple(vertex)
    return vertex[1] > 1
end
function update_left(gridmap::Gridmap2D, vertex::CartesianIndex{2})
    # cx, cy = Tuple(vertex)
    return CartesianIndex(vertex[1] - 1, vertex[2])
end

function has_right(gridmap::Gridmap2D, vertex::CartesianIndex{2})::Bool
    cx, cy = Tuple(vertex)
    return cx < getwidthcells(gridmap)
end
function update_right(gridmap::Gridmap2D, vertex::CartesianIndex{2})
    # cx, cy = Tuple(vertex)
    return CartesianIndex(vertex[1] + 1, vertex[2])
end

function has_up(gridmap::Gridmap2D, vertex::CartesianIndex{2})::Bool
    # cx, cy = Tuple(vertex)
    return vertex[2] > 1
end
function update_up(gridmap::Gridmap2D, vertex::CartesianIndex{2})
    # cx, cy = Tuple(vertex)
    return CartesianIndex(vertex[1], vertex[2] - 1)
end

function has_down(gridmap::Gridmap2D, vertex::CartesianIndex{2})::Bool
    # cx, cy = Tuple(vertex)
    return vertex[2] < getheightcells(gridmap)
end
function update_down(gridmap::Gridmap2D, vertex::CartesianIndex{2})
    # cx, cy = Tuple(vertex)
    return CartesianIndex(vertex[1], vertex[2] + 1)
end

"""
    on_map(gridmap::Gridmap2D, vertex::CartesianIndex{2})::Bool
    on_map(gridmap::Gridmap2D, point::Point2D)::Bool

Check if `vertex` is within the bounds of `gridmap`.
"""
function on_map(gridmap::Gridmap2D, vertex::CartesianIndex{2})::Bool
    # cx, cy = Tuple(vertex)
    return (vertex[1] >= 1) && (vertex[1] <= getwidthcells(gridmap)) && (vertex[2] >= 1) && (vertex[2] <= getheightcells(gridmap))
end
function on_map(gridmap::Gridmap2D, point::Point2D)::Bool
    origin = get_origin(gridmap)

    tol = 1e-6
    return (point.x >= origin.x - tol) && (point.x <= origin.x + getwidthmetric(gridmap) + tol) &&
           (point.y >= origin.y - tol) && (point.y <= origin.y + getheightmetric(gridmap) + tol)
end

"""
    get_neighbours_4(gridmap::Gridmap2D, vertex::CartesianIndex{2})

**Precondition:** `vertex` is on the `gridmap`.

Get the NSEW neighbours of `vertex`.

Return: `Vector{CartesianIndex{2}}`
"""
function get_neighbours_4(gridmap::Gridmap2D, vertex::CartesianIndex{2})
    if !on_map(gridmap, vertex)
        throw(BoundsError(gridmap.data, vertex))
    end
    neighbours = Vector{CartesianIndex{2}}()
    if has_left(gridmap, vertex)
        push!(neighbours, update_left(gridmap, vertex))
    end
    if has_right(gridmap, vertex)
        push!(neighbours, update_right(gridmap, vertex))
    end
    if has_up(gridmap, vertex)
        push!(neighbours, update_up(gridmap, vertex))
    end
    if has_down(gridmap, vertex)
        push!(neighbours, update_down(gridmap, vertex))
    end
    return neighbours
end

"""
    get_neighbours_8(gridmap::Gridmap2D, vertex::CartesianIndex{2})

**Precondition:** `vertex` is on the `gridmap`.

Get all adjacent neighbours of `vertex`.

Return: `Vector{CartesianIndex{2}}`
"""
function get_neighbours_8(gridmap::Gridmap2D{DataType, CoordinateType}, vertex::CartesianIndex{2}) where {DataType, CoordinateType}
    if !on_map(gridmap, vertex)
        throw(BoundsError(gridmap.data, vertex))
    end
    neighbours = Vector{CartesianIndex{2}}()
    has_left_neighbour = has_left(gridmap, vertex)
    has_right_neighbour = has_right(gridmap, vertex)
    has_down_neighbour = has_down(gridmap, vertex)
    has_up_neighbour = has_up(gridmap, vertex)
    if has_left_neighbour
        push!(neighbours, update_left(gridmap, vertex))
    end
    if has_right_neighbour
        push!(neighbours, update_right(gridmap, vertex))
    end
    if has_up_neighbour
        push!(neighbours, update_up(gridmap, vertex))
    end
    if has_down_neighbour
        push!(neighbours, update_down(gridmap, vertex))
    end
    if has_left_neighbour && has_up_neighbour
        push!(neighbours, update_up(gridmap, update_left(gridmap, vertex)))
    end
    if has_left_neighbour && has_down_neighbour
        push!(neighbours, update_down(gridmap, update_left(gridmap, vertex)))
    end
    if has_right_neighbour && has_up_neighbour
        push!(neighbours, update_up(gridmap, update_right(gridmap, vertex)))
    end
    if has_right_neighbour && has_down_neighbour
        push!(neighbours, update_down(gridmap, update_right(gridmap, vertex)))
    end
    return neighbours
end

"""
    get_neighbours(gridmap::Gridmap2D, vertex::CartesianIndex{2})

Default searches used in functions -- defaults to [`get_neighbours_8`](@ref).
"""
function get_neighbours(gridmap::Gridmap2D, vertex::CartesianIndex{2})
    return get_neighbours_8(gridmap, vertex)
end

"""
    is_traversable(gridmap::Gridmap2D, index::CartesianIndex)

Return `true` if the cell on the gridmap is traversable according to the particular implementation.
Default to [`is_safe`](@ref).
"""
is_traversable(gridmap::Gridmap2D, index::CartesianIndex) = is_safe(gridmap, index)

"""
    is_safe(gridmap::Gridmap2D, index::CartesianIndex)

Return `true` if the cell on the gridmap is safe, i.e. it is free of any obstacles.
"""
is_safe(gridmap::Gridmap2D, index::CartesianIndex) = gridmap.data[index] == Mapping.FREE
"""
    is_safe_or_unknown(gridmap::Gridmap2D, index::CartesianIndex)

Return `true` if the cell on the gridmap is obstacle-free or unknown.
"""
is_safe_or_unknown(gridmap::Gridmap2D, index::CartesianIndex) = (gridmap.data[index] == Mapping.FREE) || (gridmap.data[index] == Mapping.NO_INFORMATION)

"""
    bfs(gridmap::Gridmap2D, s::Point2D, d::Point2D;
        get_neighbours = get_neighbours, is_traversable = is_safe)

BFS on a `gridmap` from the start, `s`, to the destination, `d`.

Optional keyword arguments include:
* `get_neighbours`: defaults to [`get_neighbours`](@ref)
* `is_traversable`: function to check if a cell is traversable. It defaults to only traversing through safe cells, e.g. only free space.
"""
function bfs(gridmap::Gridmap2D, s::Point2D, d::Point2D;
             get_neighbours = get_neighbours, is_traversable = is_safe)
    if !on_map(gridmap, s)
        throw(BoundsError("$s is not on the gridmap."))
    end
    if !on_map(gridmap, d)
        throw(BoundsError("$d is not on the gridmap."))
    end

    start_ind = get_cartesian_index(gridmap, s)
    end_ind = get_cartesian_index(gridmap, d)

    previous = Dict{CartesianIndex, CartesianIndex}()
    q = ds.Queue{CartesianIndex}()
    ds.enqueue!(q, start_ind)
    while !isempty(q)
        current_cell = ds.dequeue!(q)
        if current_cell == end_ind
            return reconstruct_path(previous, start_ind, end_ind), Set(keys(previous))
        end
        neighbours = get_neighbours(gridmap, current_cell)
        for neighbour ∈ neighbours
            if is_traversable(gridmap, neighbour) && (!haskey(previous, neighbour))
                previous[neighbour] = current_cell
                ds.enqueue!(q, neighbour)
            end
        end
    end

    return nothing, previous
end

function reconstruct_path(previous::Dict{CartesianIndex, CartesianIndex}, start_ind::CartesianIndex, end_ind::CartesianIndex)
    path = [end_ind]
    while path[end] != start_ind
        push!(path, previous[path[end]])
    end
    cell_path = CellPath2D()
    cell_path.indices = reverse(path)
    return cell_path
end

"""
    indices2position(gridmap::Gridmap2D, cell_path::CellPath2D)
    indices2position(gridmap::Gridmap2D, path::Vector{CartesianIndex{2}})

Helper function to convert cartesian indices to a [`Path2D`](@ref).
"""
function indices2position(gridmap::Gridmap2D, path::Vector{CartesianIndex{2}})
    new_path = Path2D()
    for cell ∈ path
        push!(new_path.points, get_position(gridmap, cell))
    end
    return new_path
end

indices2position(gridmap::Gridmap2D, cell_path::CellPath2D) = indices2position(gridmap, cell_path.indices)

function null_heuristic(start::PointND{N, T}, goal) where {N, T}
    return T(0)
end

"""
    manhattan_distance(start, goal)

Returns the Manhattan distance between `start` and `goal`. For this method to work, `start` and `goal` must:
* have the operator `-`, i.e. subtraction, defined
* be callable with `Tuple`, e.g. `Tuple(start)`
"""
function manhattan_distance(start, goal)
    sum(abs.(Tuple(goal - start)))
end

function priority_search(gridmap::Gridmap2D, s::Point2D, d::Point2D;
                         heuristic = null_heuristic,
                         get_neighbours = get_neighbours,
                         is_traversable = is_safe)
    if !on_map(gridmap, s)
        throw(BoundsError("$s is not on the gridmap."))
    end
    if !on_map(gridmap, d)
        throw(BoundsError("$d is not on the gridmap."))
    end

    start_ind = get_cartesian_index(gridmap, s)
    end_ind = get_cartesian_index(gridmap, d)

    pq = ds.PriorityQueue{CartesianIndex, Real}()
    ds.enqueue!(pq, start_ind, 0)
    previous = Dict{CartesianIndex, CartesianIndex}()
    sizehint!(previous, length(gridmap.data))
    cost_so_far = Dict{CartesianIndex, Real}()
    sizehint!(cost_so_far, length(gridmap.data))
    cost_so_far[start_ind] = 0
    while !isempty(pq)
        current_cell = ds.dequeue!(pq)
        if current_cell == end_ind
            return reconstruct_path(previous, start_ind, end_ind), Set(keys(previous))
        end
        neighbours = get_neighbours(gridmap, current_cell)
        for neighbour ∈ neighbours
            neighbour_point = get_position(gridmap, neighbour)
            new_cost = cost_so_far[current_cell] + dist(neighbour_point, get_position(gridmap, current_cell))
            if is_traversable(gridmap, neighbour) && (!haskey(cost_so_far, neighbour) || (new_cost < cost_so_far[neighbour]))
                cost_so_far[neighbour] = new_cost
                previous[neighbour] = current_cell
                pq[neighbour] = new_cost + heuristic(neighbour_point, d)
            end
        end
    end
    return nothing, Set(keys(previous))
end

"""
    dijkstra(gridmap::Gridmap2D, s::Point2D, d::Point2D;
             get_neighbours = get_neighbours, is_traversable = is_safe)

Dijkstra on a `gridmap` from the start, `s`, to the destination, `d`.

Optional keyword arguments include:
* `get_neighbours`: defaults to [`get_neighbours`](@ref)
* `is_traversable`: function to check if a cell is traversable. It defaults to only traversing through safe cells, e.g. only free space.
"""
function dijkstra(gridmap::Gridmap2D, s::Point2D, d::Point2D;
                  get_neighbours = get_neighbours,
                  is_traversable = is_safe)

    return priority_search(gridmap, s, d,
                           heuristic = null_heuristic,
                           get_neighbours = get_neighbours,
                           is_traversable = is_traversable)
end

"""
    astar(gridmap::Gridmap2D, s::Point2D, d::Point2D;
          get_neighbours = get_neighbours, is_traversable = is_safe)

A* on a `gridmap` from the start, `s`, to the destination, `d`.

Optional keyword arguments include:
* `get_neighbours`: defaults to [`get_neighbours`](@ref)
* `is_traversable`: function to check if a cell is traversable. It defaults to only traversing through safe cells, e.g. only free space.
"""
function astar(gridmap::Gridmap2D, s::Point2D, d::Point2D;
               heuristic = manhattan_distance,
               get_neighbours = get_neighbours,
               is_traversable = is_safe)

    return priority_search(gridmap, s, d,
                           heuristic = heuristic,
                           get_neighbours = get_neighbours,
                           is_traversable = is_traversable)
end
